let data, attempts; 

window.onload = function() {
    document.querySelector('select').onchange = function() {
        selectOption(this.value);
    };  

    document.querySelector('input').onkeypress = function(e) {
        if(!e) e = window.event;
        if(e.keyCode == '13') {
            checkAnswer(this.value);
            this.value = ""; 
            return false;
        }
    }

    initialize(); 
};

function initialize() {
    document.querySelector('#result').innerHTML = "";
    
    let selection = document.querySelector('select').value;
    if(selection) selectOption(selection); 
}

function selectOption(option) {
    if(option == "pokemon") {
        fetch("https://pokeapi.co/api/v2/pokemon-species")
            .then(response => response.json())
            .then(json => {
                getRandom = getRandom.bind({
                    url: "https://pokeapi.co/api/v2/pokemon/",
                    count: json.count
                });
                getRandom();
            });
    } else {
        console.log("Other options don't function yet.");
    }
}

function getRandom() {
    try {
        let id = (Math.floor(Math.random() * this.count) + 1);
        fetch(this.url + id)
            .then(response => response.json())
            .then(json => {
                data = json; 
                document.querySelector('img').src = data.sprites.back_default || data.sprites.front_default;
                attempt(true);
            });
    } catch(e) {
        getRandom(); 
    }
}

function attempt(initialize) {
    if(initialize) {
        attempts = 0; 
    } else {
        attempts++; 
    }

    document.querySelector('#attempts').innerHTML = attempts; 
}

function checkAnswer(answer) {
    attempt(); 
    if(answer == data.id || answer.toLowerCase() == data.name) {
        printResult(true); 
        if(confirm("You have become the very best! Kinda. Try again?")) initialize(); 
    } else {
        printResult(false, !isNaN(answer) ? answer > data.id : undefined); 
    }
}

function printResult(correct, high) {
    let result = "Wrong!"; 
    if(correct) {
        result = "Correct!"; 
    } else if(high != undefined) {
        if(high) {
            result += " Your answer is too high.";
        } else {
            result += " Your answer is too low.";
        }
    }

    document.querySelector('#result').innerHTML = result; 
}
